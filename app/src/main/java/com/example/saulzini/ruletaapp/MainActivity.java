package com.example.saulzini.ruletaapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.*;
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView=null;
    Toolbar toolbar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initiar el fragmento

        ListaNumeros listaNumeros = new ListaNumeros();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        //Reemplazar fragmento
        fragmentTransaction.replace(R.id.fragment_container,listaNumeros);
        fragmentTransaction.commit();


         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                */
                //initiar el fragmento

                IngresarNumero ingresarNumero = new IngresarNumero();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                //Reemplazar fragmento
                fragmentTransaction.replace(R.id.fragment_container,ingresarNumero);
                fragmentTransaction.commit();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            VariablesGlobales.getInstance().listaNumeros.clear();

            //initiar el fragmento

            ListaNumeros listaNumeros = new ListaNumeros();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //Reemplazar fragmento
            fragmentTransaction.replace(R.id.fragment_container,listaNumeros);
            fragmentTransaction.commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.listaNumeros) {
            //initiar el fragmento

            ListaNumeros listaNumeros = new ListaNumeros();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //Reemplazar fragmento
            fragmentTransaction.replace(R.id.fragment_container,listaNumeros);
            fragmentTransaction.commit();

        } else if (id == R.id.Numeros) {
            //initiar el fragmento

            BinomialNumeros binomialNumeros= new BinomialNumeros();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //Reemplazar fragmento
            fragmentTransaction.replace(R.id.fragment_container,binomialNumeros);
            fragmentTransaction.commit();

        } else if (id == R.id.Docenas) {

            BinomialDocenas listaNumeros = new BinomialDocenas();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //Reemplazar fragmento
            fragmentTransaction.replace(R.id.fragment_container,listaNumeros);
            fragmentTransaction.commit();



        } else if (id == R.id.Colores) {

            BinomialColores listaNumeros = new BinomialColores();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //Reemplazar fragmento
            fragmentTransaction.replace(R.id.fragment_container,listaNumeros);
            fragmentTransaction.commit();

        } else if (id == R.id.Filas) {

            BinomialFilas listaNumeros = new BinomialFilas();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //Reemplazar fragmento
            fragmentTransaction.replace(R.id.fragment_container,listaNumeros);
            fragmentTransaction.commit();

        } else if (id == R.id.PareImpar) {

            BinomialParEImpar listaNumeros = new BinomialParEImpar();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //Reemplazar fragmento
            fragmentTransaction.replace(R.id.fragment_container,listaNumeros);
            fragmentTransaction.commit();

        }

        else if (id == R.id.mitades) {

            BinomialMitades listaNumeros = new BinomialMitades();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //Reemplazar fragmento
            fragmentTransaction.replace(R.id.fragment_container,listaNumeros);
            fragmentTransaction.commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
