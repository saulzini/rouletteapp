package com.example.saulzini.ruletaapp;


import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;


/**
 * A simple {@link Fragment} subclass.
 */
public class BinomialNumeros extends Fragment {

    int numeroLimite = 0;

    public BinomialNumeros() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.binomial_numeros, container, false);
    }


    public void crearInterfaz(double arreglo[][]){

        //crear
        TableLayout numeros= (TableLayout)getActivity().findViewById(R.id.tabla);
        numeros.removeAllViews();

        numeros.setStretchAllColumns(true);
        numeros.bringToFront();


       //Encabezado
        TableRow encabezado =  new TableRow(getActivity());

        TextView posicion = new TextView(getActivity());
        posicion.setText(R.string.posicion);
        posicion.setTextColor(Color.WHITE);
        posicion.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        //padding con 20 para el texto
        int paddingPixel = 20;
        float density = getActivity().getResources().getDisplayMetrics().density;
        int paddingDp = (int)(paddingPixel * density);
        posicion.setPadding(0, paddingDp, 0, 0);
        posicion.setGravity(Gravity.CENTER);


        TextView encabezadoc1 = new TextView(getActivity());
        encabezadoc1.setText(R.string.numero);
        encabezadoc1.setTextColor(Color.WHITE);
        encabezadoc1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        encabezadoc1.setPadding(0, paddingDp, 0, 0);
        encabezadoc1.setGravity(Gravity.CENTER);

        TextView encabezadoc2 = new TextView(getActivity());
        encabezadoc2.setText(R.string.probabilidad);
        encabezadoc2.setTextColor(Color.WHITE);
        encabezadoc2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        encabezadoc2.setPadding(0, paddingDp, 0, 0);
        encabezadoc2.setGravity(Gravity.CENTER);

        encabezado.setWeightSum(1);
        encabezado.addView(posicion);
        encabezado.addView(encabezadoc1);
        encabezado.addView(encabezadoc2);

        numeros.addView(encabezado);


        for(int i = 0; i < numeroLimite; i++){
            TableRow tr =  new TableRow(getActivity());


            TextView posiciones = new TextView(getActivity());
            posiciones.setText(Integer.toString(i + 1));
            posiciones.setTextColor(Color.WHITE);
            posiciones.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            posiciones.setPadding(0, paddingDp, 0, 0);
            posiciones.setGravity(Gravity.CENTER);



            TextView c1 = new TextView(getActivity());

            c1.setText(Integer.toString((int) arreglo[i][0]));
            c1.setTextColor(Color.WHITE);
            c1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            c1.setPadding(0, paddingDp, 0, 0);
            c1.setGravity(Gravity.CENTER);

            TextView c2 = new TextView(getActivity());
            c2.setText(String.format("%.5f", arreglo[i][1]) + " %");
            c2.setTextColor(Color.WHITE);
            c2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            c2.setPadding(0, paddingDp, 0, 0);
            c2.setGravity(Gravity.CENTER);

            tr.setWeightSum(1);
            tr.addView(posiciones);
            tr.addView(c1);
            tr.addView(c2);

            numeros.addView(tr);
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        if (VariablesGlobales.getInstance().listaNumeros.contains(37) ){
            numeroLimite=38;

        }
        else {
            numeroLimite=37;
        }

        Collection<Integer> collection = new ArrayList<Integer>(VariablesGlobales.getInstance().listaNumeros);

        int frecuencias[] = new int[numeroLimite];
        double distribucionBinomial[][] = new double[numeroLimite][2];
        double probabilidadNumero = 1.0/numeroLimite;


        for(int i=0; i<numeroLimite; i++){


            //obtener frecuencias
            frecuencias[i] = Collections.frequency(collection, i);

            //obtener distribuciones
            distribucionBinomial[i][0]= i;


            distribucionBinomial[i][1]= VariablesGlobales.DistribucionBinomial(VariablesGlobales.getInstance().listaNumeros.size(), frecuencias[i], probabilidadNumero)*100;

        }


        //Ordenar con respecto a la segunda columna
        Arrays.sort(distribucionBinomial, new Comparator<double[]>() {
            @Override
            public int compare(double[] o1, double[] o2) {
                return Double.compare(o2[1], o1[1]);
            }
        });

        crearInterfaz(distribucionBinomial);


    }
}
