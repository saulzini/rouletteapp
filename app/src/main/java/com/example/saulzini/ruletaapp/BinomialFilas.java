package com.example.saulzini.ruletaapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Comparator;


/**
 * A simple {@link Fragment} subclass.
 */
public class BinomialFilas extends Fragment {



    TextView textos[][] = new TextView[4][4];

    public BinomialFilas() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_binomial_filas, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();


        int frecuencias[] = new int[4];
        double distribucionBinomial[][] = new double[4][2];
        double probabilidadNumero = 1.0/4;


        for(int i=0; i<VariablesGlobales.getInstance().listaNumeros.size(); i++){
            int numero = VariablesGlobales.getInstance().listaNumeros.get(i);

            if ( numero >= 1 && numero <= 36  ) {

                if(numero % 3==1){
                    //primer fila
                    frecuencias[1] = frecuencias[1]+1;
                }
                else if (numero %3 ==2){
                    //segunda fila
                    frecuencias[2] = frecuencias[2]+1;
                }

                else if(numero %3 == 0){
                    //tercera fila
                    frecuencias[0] = frecuencias[0]+1;
                }


            }
            else {
                frecuencias[3] = frecuencias[3]+1;
            }


        }

        for(int i=0; i<4; i++){


            //obtener distribuciones
            distribucionBinomial[i][0]= i;
            distribucionBinomial[i][1]= VariablesGlobales.DistribucionBinomial(VariablesGlobales.getInstance().listaNumeros.size(), frecuencias[i], probabilidadNumero)*100;

        }

        //Ordenar con respecto a la segunda columna
        Arrays.sort(distribucionBinomial, new Comparator<double[]>() {
            @Override
            public int compare(double[] o1, double[] o2) {
                return Double.compare(o2[1], o1[1]);
            }
        });

        textos[0][0] =  (TextView) getActivity().findViewById(R.id.textView00);
        textos[1][0] =  (TextView) getActivity().findViewById(R.id.textView10);
        textos[2][0] =  (TextView) getActivity().findViewById(R.id.textView20);
        textos[3][0] =  (TextView) getActivity().findViewById(R.id.textView30);

        textos[0][1] =  (TextView) getActivity().findViewById(R.id.textView01);
        textos[1][1] =  (TextView) getActivity().findViewById(R.id.textView11);
        textos[2][1] =  (TextView) getActivity().findViewById(R.id.textView21);
        textos[3][1] =  (TextView) getActivity().findViewById(R.id.textView31);


        for(int i=0; i<4; i++){

            textos[i][0].setTextColor(Color.WHITE);
            textos[i][0].setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

            switch ((int) distribucionBinomial[i][0]){
                case 0:
                    textos[i][0].setText(R.string.terceraFila);
                    break;
                case 1:
                    textos[i][0].setText(R.string.primeraFila);
                    break;
                case 2:
                    textos[i][0].setText(R.string.segundaFila);
                    break;
                case 3:
                    textos[i][0].setText(R.string.none);
                    break;
            }

            textos[i][1].setTextColor(Color.WHITE);
            textos[i][1].setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            textos[i][1].setText(String.format("%.5f", distribucionBinomial[i][1]) + " %");
        }



    }
}
