package com.example.saulzini.ruletaapp;



import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class IngresarNumero extends Fragment implements View.OnClickListener {

    Button botonAnadir;
    TextView resultado;
    EditText numIngresar;

    public IngresarNumero() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ingresar_numero, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        botonAnadir = (Button) getActivity().findViewById(R.id.botonAnadir);
        resultado = (TextView) getActivity().findViewById(R.id.textViewResultado);
        numIngresar = (EditText) getActivity().findViewById(R.id.numeroIngresar);
        botonAnadir.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int dato = Integer.parseInt(numIngresar.getText().toString());

        switch (v.getId()) {

            case R.id.botonAnadir:

                if (dato >= 38) {

                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Inserte un numero menor a 37");


                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            //here you can add functions

                        }
                    });
                    alertDialog.show();

                }
                else {


                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle(R.string.numeroAgregado);
                    VariablesGlobales.getInstance().listaNumeros.add(dato);




                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            //initiar el fragmento

                            ListaNumeros listaNumeros = new ListaNumeros();
                            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            //Reemplazar fragmento
                            fragmentTransaction.replace(R.id.fragment_container,listaNumeros);
                            fragmentTransaction.commit();

                            //cerrar teclado
                            InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(numIngresar.getWindowToken(), 0);

                        }
                    });
                    alertDialog.show();



                }
                break;
        }


    }
}