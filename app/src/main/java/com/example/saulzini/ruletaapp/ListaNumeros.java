package com.example.saulzini.ruletaapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListaNumeros extends Fragment {


    public ListaNumeros() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        /*
        VariablesGlobales.getInstance().listaNumeros.add(1);
        VariablesGlobales.getInstance().listaNumeros.add(4);
        VariablesGlobales.getInstance().listaNumeros.add(9);
        VariablesGlobales.getInstance().listaNumeros.add(11);
        VariablesGlobales.getInstance().listaNumeros.add(14);

        VariablesGlobales.getInstance().listaNumeros.add(20);
        VariablesGlobales.getInstance().listaNumeros.add(18);
        VariablesGlobales.getInstance().listaNumeros.add(27);
        VariablesGlobales.getInstance().listaNumeros.add(30);
        VariablesGlobales.getInstance().listaNumeros.add(34);
        VariablesGlobales.getInstance().listaNumeros.add(37);
           */
     /*   Random numero = new Random();
        for (int i=0; i< 600; i++){
            VariablesGlobales.getInstance().listaNumeros.add(numero.nextInt(38));

        }
        */
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lista_numeros, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Añadir a la lista lo que se tiene en el arreglo global
        Integer[] numeros = new Integer[VariablesGlobales.getInstance().listaNumeros.size()];
        VariablesGlobales.getInstance().listaNumeros.toArray(numeros);

        //Adaptando los numeros
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(getActivity()
                .getApplicationContext(), android.R.layout.simple_list_item_1,numeros);

        //Obteniendo la lista de la vista
        ListView listView = (ListView) getActivity().findViewById(R.id.listView);

        // llenando el arreglo para que se muestre en el arreglo
        listView.setAdapter(adapter);


    }
}
