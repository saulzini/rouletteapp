package com.example.saulzini.ruletaapp;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;


import com.google.common.math.BigIntegerMath;
import com.google.common.math.DoubleMath;
import com.google.common.math.IntMath;

/**
 * Created by saulzini on 07/01/16.
 */
public class VariablesGlobales {
    private static VariablesGlobales ourInstance = new VariablesGlobales();

    ArrayList<Integer> listaNumeros = new ArrayList<Integer>();



    public static VariablesGlobales getInstance() {

        return ourInstance;
    }

    private VariablesGlobales() {
    }

    public static double Combinacion(int n,int r){

        BigDecimal resultado = new BigDecimal(BigIntegerMath.factorial(n).divide((BigIntegerMath.factorial(r).multiply(BigIntegerMath.factorial(n - r)))));
        return resultado.doubleValue();
    }

    public static double DistribucionBinomial(int n, int k, double p){
        double probabilidad=0;
        double q = 1.0-p;

            probabilidad=Combinacion(n,k)*Math.pow(p, k)* Math.pow(q,n-k);
        return probabilidad;
    }


}
